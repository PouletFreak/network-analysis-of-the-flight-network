import operator 
import csv
import random
import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt
import networkx as nx
import numpy as np
from mpl_toolkits.basemap import Basemap

def setup(node_file, starter):
    G = nx.DiGraph()
    labels = {}
    
    with open(node_file, "r") as data:
        reader = csv.reader(data, delimiter = ' ')
        new_edges = []
        for row in reader:
            new_edges.append((row[0], row[1], row[2]))
        G.add_weighted_edges_from(new_edges)
    
    nx.set_node_attributes(G, False, 'infected')
    nx.set_node_attributes(G, False, 'healed')
    nx.set_node_attributes(G, ' ', 'since')

    G.node[str(starter)]['infected'] = True
    # degrees = G.degree(G.nodes())
    # sorted_x = sorted(degrees, key=operator.itemgetter(1), reverse=True)
    # print(sorted_x)
    return G
    

def will_heal(graph, healrate, node):

    if graph.node[node]['infected'] == True:
        rand = random.random()
        if rand <= healrate:
            return True
    return False            


def infect(graph, rate, cycle, healrate):

    suc = []
    out = []
    
    for node in graph.nodes():
        if graph.node[node]['infected'] == True:
            heal = will_heal(graph, healrate, node)
            if heal:
                graph.node[node]['infected'] = False
                graph.node[node]['healed'] = True
    
    for node in graph.nodes():
        if graph.node[node]['infected'] == True:
            successors = graph.successors(node)
            out_edges = graph.out_edges(node)
            for edge in out_edges:
                out.append(edge)
                # print(edge)
            for n in successors:
                suc.append(n)
        
    for node in graph.nodes():
        if node in suc:
            for edge in out:
                if edge[1] == node:
    
                    will_inf = will_infect(rate, graph, edge, node)    
    
                    if will_inf:
                        graph.node[node]['infected'] = True
                        graph.node[node]['since'] = cycle + 1
                        

def will_infect(rate, graph, edge, node):
    weight = int(graph.edges[edge]['weight'])
    if graph.node[node]['healed'] == True:
        return False
    
    rand = random.random()
    for i in range(weight):
        if rand <= rate:
            return True
    
    return False


def remove_x_successors_nodes(G, node, num_succ):
    successors = G.successors(str(node))
    degrees = G.degree(successors)
    degrees_dict = dict(degrees)
    
    sorted_x = sorted(degrees_dict.items(), key=operator.itemgetter(1), reverse=True)
    print('Number successors: {}'.format(len(sorted_x)))
    counter = 0
    for k, v in sorted_x:
        G.remove_node(str(k))
        counter += 1
        if counter >= num_succ:
            break
    print('New number of nodes: {}'.format(G.number_of_nodes()))
    print('New number of edges: {}'.format(G.number_of_edges()))


def remove_x_global_nodes(G, starter, num_succ):
    degrees = G.degree()
    
    sorted_x = sorted(degrees, key=operator.itemgetter(1), reverse=True)
    counter = 0
    for k, v in sorted_x:
        if str(starter) != str(k):
            G.remove_node(str(k))
            counter += 1
            if counter >= num_succ:
                break

    print('----')
    print('New number of nodes: {}'.format(G.number_of_nodes()))
    print('New number of edges: {}'.format(G.number_of_edges()))
    print('Strongly connected components: {}'.format(nx.number_strongly_connected_components(G)))
    print('----')



def remove_x_successors(G, node, num_hubs):
    successors = G.successors(str(node))
    degrees = G.degree(successors)
    degrees_dict = dict(degrees)
    
    sorted_x = sorted(degrees_dict.items(), key=operator.itemgetter(1), reverse=True)
    print('Number successors: {}'.format(len(sorted_x)))
    counter = 0
    for k, v in sorted_x:
        G.remove_edge(str(node), str(k))
        counter += 1
        if counter >= num_hubs:
            break
    print('New number of edges: {}'.format(G.number_of_edges()))
    print('Strongly connected components: {}'.format(nx.number_strongly_connected_components(G)))

def plot_histo(num_iterations, results_infected, results_healed, results_healthy, aniname, over_150):

    plt.bar(range(num_iterations), results_infected,  alpha=0.5, label='# infected')
    plt.bar(range(num_iterations), results_healed,  alpha=0.5, label='# healed')
    plt.bar(range(num_iterations), results_healthy,  alpha=0.1, label='# healthy')
    plt.legend(loc='lower right')
    plt.savefig('{}.png'.format(aniname), dpi=300)
    plt.clf()

    try:
        if over_150:
            print('{} Cycle with over 150 infections: {}'.format(aniname, over_150[0]))
        # print('{}'.format(results_healthy[-1]))
    except: 
        pass

def remove_x_betweenness(G, starter, num):
    betw = nx.betweenness_centrality(G)
    sorted_betw = sorted(betw.items(), key=operator.itemgetter(1), reverse=True)
    
    counter = 0
    for k,v in sorted_betw:
        if str(k) != str(starter):
            G.remove_node(str(k))
            counter += 1
            if counter >= num:
                break

    print('----')
    print('New number of nodes: {}'.format(G.number_of_nodes()))
    print('New number of edges: {}'.format(G.number_of_edges()))
    print('Strongly connected components: {}'.format(nx.number_strongly_connected_components(G)))
    print('----')
    

def print_betweenness(G):
    betw = nx.betweenness_centrality(G)
    sorted_betw = sorted(betw.items(), key=operator.itemgetter(1), reverse=True)
    #print(sorted_betw[0:15])
    fifty_betw = sorted_betw[0:50]

    degs = G.degree()
    sorted_degs = sorted(degs, key=operator.itemgetter(1), reverse=True)
    #print(sorted_degs[0:50])
    
    deg_betw = []

    for k,v in fifty_betw:
        for x,y in sorted_degs:
            if str(k) == str(x):
                deg_betw.append((k,v,y))
    print(deg_betw)
        


def save_animation_pngs(num_iterations, node_file, starter, aniname, rate, healrate, num_to_remove):
    G = setup(node_file, starter)
    # print_betweenness(G)
    #for g in nx.weakly_connected_component_subgraphs(G): 
    #   print(nx.average_shortest_path_length(g))
    #    print(g.number_of_nodes())
    #print(nx.average_clustering(G))
    # UG = G.to_undirected()
    # r=nx.degree_assortativity_coefficient(UG)
    # print('asso: {}'.format(r))
    
    #degrees = G.degree()
    #degrees_dict = dict(degrees)
    
    #sorted_x = sorted(degrees_dict.items(), key=operator.itemgetter(1), reverse=True)
    #print('Max: {}, Min: {}'.format(sorted_x[0], sorted_x[-1]))
    #avg_deg = 0
    #for k,v in sorted_x:
    #    avg_deg += v
    #print('avg: {}'.format(avg_deg / len(sorted_x)))
    #print(nx.info(G))
    num_nodes = G.number_of_nodes() 
    # print(G.number_of_edges())
    # remove_x_successors_nodes(G, starter, num_to_remove)
    # remove_x_global_nodes(G, starter, num_to_remove)
    remove_x_betweenness(G, starter, num_to_remove)
    # remove_x_successors(G,starter,num_to_remove)
    print('Strongly connected components: {}'.format(nx.number_strongly_connected_components(G)))
    # print('New number of edges: {}'.format(G.number_of_edges()))
    results_healed = []
    results_infected = []
    over_150 = []

    results_healed.append(0)
    results_infected.append(1/num_nodes)

    # print('Is Weakly connected: {}'.format(nx.is_weakly_connected(G)))
    # print('Weakly connected components: {}'.format(nx.number_weakly_connected_components(G)))
    # print('Is Strongly connected: {}'.format(nx.is_strongly_connected(G)))
    # print('Strongly connected components: {}'.format(nx.number_strongly_connected_components(G)))

    # comps = nx.weakly_connected_component_subgraphs(G, copy=False)
    # counter = 0
    # for g in comps:
    #     num_o = g.number_of_nodes()
    #     print('Component {}: {}'.format(counter, num_o))
    #     counter += 1
    
    for i in range(0,num_iterations-1):
        infect(G, rate, i, healrate)        

        num_infected = len([x for x,y in G.nodes(data=True) if y['infected'] == True])
        num_healed = len([x for x,y in G.nodes(data=True) if y['healed'] == True])
        if num_infected >= 150:
            over_150.append(i+1)
        # print('Inf: {}'.format(num_infected))
        results_healed.append(num_healed/num_nodes)
        results_infected.append(num_infected/num_nodes)
            
    results_healthy = []

    for i in range(len(results_healed)):
        num_healthy = (num_nodes - (results_infected[i]*num_nodes) - (results_healed[i]*num_nodes)) / float(num_nodes)
        results_healthy.append(num_healthy)
        # print('{}: {}'.format(i, num_healthy))

    if num_healed == 1 or num_healed == 0:
        pass
    else:
        # plot_histo(num_iterations, results_infected, results_healed, results_healthy, aniname, over_150)
        pass

    return results_infected, results_healed, results_healthy
    
if __name__ == "__main__":

    steps_plot = 125

    total_infected = [0 for i in range(0,steps_plot)]
    total_healed = [0 for i in range(0,steps_plot)]
    total_healthy = [0 for i in range(0,steps_plot)]

    itera = 50 
    av = itera    

    for i in range(0,itera):

        results_infected, results_healed, results_healthy = save_animation_pngs(steps_plot,'openflights_na.txt', 340, 'ani-frankfurt-infect0.01-heal0.1-0hubs-conn-removed', 0.15, 0.07, 78) # Animation cycles, geofile, startnode, infection rate, heal rate, number of successor hubs to remove
        if i % 10 == 0:
            print('Durchgang: {}'.format(i))
        if results_healed[-1] == 1:
            print('Ding!')
            av = av - 1
            continue

        vec1 = np.array(total_infected)
        vec2 = np.array(results_infected)
        total_infected = vec1 + vec2

        vec1 = np.array(total_healed)
        vec2 = np.array(results_healed)
        total_healed = vec1 + vec2

        vec1 = np.array(total_healthy)
        vec2 = np.array(results_healthy)
        total_healthy = vec1 + vec2

    print('iterations: {}'.format(av))
        
    vector1 = np.array(total_infected)
    average_infected = vector1 / av 

    vector2 = np.array(total_healed)
    average_healed = vector2 / av

    vector3 = np.array(total_healthy)
    average_healthy = vector3 / av

    over_150 = []
    plot_histo(steps_plot, average_infected, average_healed, average_healthy, "Frankfurt-p15-h7-118hubs-betweenness-nodes-removed", over_150)
    with open('results-118-hubs-nodes-betweenness-rem.txt', 'a') as f:
        f.write('Average infected: \n')
        f.write('[') 
        counter = 0
        for i in average_infected:
            f.write('{}: {}, '.format(counter, round(i, 5)))
            counter += 1
        f.write(']\n')
    
        f.write('Average healed: \n')
        f.write('[') 
        counter = 0
        for i in average_healed:
            f.write('{}: {}, '.format(counter, round(i, 5)))
            counter += 1
        f.write(']\n')

        f.write('Average healthy: \n')
        f.write('[') 
        counter = 0
        for i in average_healthy:
            f.write('{}: {}, '.format(counter, round(i, 5)))
            counter += 1
        f.write(']\n')
